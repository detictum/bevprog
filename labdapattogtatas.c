#include <ncurses.h>
#include <unistd.h>
#include <stdlib.h>


int main(){

	int lines = 24 * 2;
	int columns = 78 * 2;
	int y1 = 0;
	int y2 = 0;
	int x1 = 0;
	int x2 = 0;

	initscr();
	
	noecho();
	cbreak();

	while(1){

		x1 = (x1 - 1) % columns;
		x2 = (x2 + 1) % columns;
		y1 = (y1 - 1) % lines;
		y2 = (y2 + 1) % lines;


		clear();
		mvaddch(abs((y1 + (lines - y2)) / 2), 
			 	 abs((x1 + (columns - x2)) / 2 ), 'O');

		refresh();
		usleep(50000);

	}


	endwin();

	return 0;
}