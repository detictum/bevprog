#include <iostream>
#include <vector>
#include <algorithm>

std::vector<int> gv = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512 };

void f(std::vector<int> v){

	std::vector<int> lv(10);
	

	for(int i = 0; i<10; ++i){
		lv[i] = v[i];
	}

	
	for(int i = 0; i<10; ++i){
		std::cout<<i+1<<". element (copy of arg values): "<<lv[i]<<std::endl;
	
	}

	std::vector<int> lv2;
	lv2 = lv;

	for(int i = 0; i<10; ++i){
		std::cout<<i+1<<". element (copy of arg): "<<lv2[i]<<std::endl;
	}

}

int fact(int n){
	if(n == 0 || n == 1){
		return 1;
	}

	return n*fact(n-1);
}





int main(){
	
	f(gv);
	std::vector<int> vv = { fact(1), fact(2), fact(3), fact(4), fact(5), fact(6), fact(7), fact(8), fact(9), fact(10) };
	f(vv);




	return 0;
}