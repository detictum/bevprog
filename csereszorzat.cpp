#include <iostream>

int main(){

    int a = 3;
    int b = 5;

    std::cout<<"Csere elott A = "<<a<<" B = "<<b<<std::endl;

    a = a*b;
    b = a/b;
    a = a/b;

    std::cout<<"Csere utan A = "<<a<<" B = "<<b<<std::endl;

    return 0;
}