#include <iostream>
#include <vector>
using namespace std;

int main(){

   /* 1.Cout << "Success!\n";     -->  the C in cout can't be a capital letter
        cout << "Success!\n";


      2.cout << "Success!\n;"       -->  the semi-colon is between ""s
        cout << "Success!\n";


      3."cout << "Success" << !\n"  -->  a semicolon is missing, cout is between ""s
        cout << "Success" << "!\n";


      4.cout << success << '\n';    -->  the string must be between ""s if it is not a variable,
        cout << "success" << '\n';


      5.string res = 7; vector<int> v(10); v[5] = res; cout << "Success!\n";
                                -->  res is a string, while we are trying to give it an int as a value,
                                     also we are trying to give a string variable to the v vector while its type is
                                     integer
        int res = 7;
        vector<int> v(10);
        v[5] = res;
        cout << "Success!\n";


      6. vector<int> v(10); v(5) = 7; if (v(5)!=7) cout << "Success!\n";
                                    --> v(5), wrong parenthesis, the if statement will fail
         vector<int> v(10);
         v[5] = 7;
         if(v[5] == 7){
            cout<<"Success!\n";
         }

       7.if (cond) cout << "Success!\n"; else cout << "Fail!\n"; --> there is no valid condition given
         bool cond = true;
         if (cond) cout << "Success!\n"; else cout << "Fail!\n";


       8.bool c = false; if (c) cout << "Success!\n"; else cout << "Fail!\n"; -->

         bool c = false;
         if (!c) cout<<Success!\n"; else cout << "Fail!\n";


       9. string s = "ape"; boo c = "fool"<s; if (c) cout << "Success!\n"; -->
            string s = "ape";
            bool c = "fool">s;
            if (c) cout << "Success!\n";



       10. string s = "ape"; if (s=="fool") cout << "Success!\n";
            string s = "ape";
            if (s != "fool") cout << "Success!\n";



       11. string s = "ape"; if (s=="fool") cout < "Success!\n";    --> there is only one '<' after cout
            string s = "ape";
            if (s != "fool") cout << "Success!\n";


       12. string s = "ape"; if (s+"fool") cout < "Success!\n";
             string s = "ape";
             if (s != "fool") cout << "Success!\n";


       13.vector<char> v(5); for (int i=0; 0<v.size(); ++i) ; cout << "Success!\n"; --> 0 will always be less than
                                                                                        v.size()

          vector<char> v(5);
        for (int i=0; i<v.size(); ++i) ;

        cout << "Success!\n";


       14.vector<char> v(5); for (int i=0; i<=v.size(); ++i) ; cout << "Success!\n"; --> it will write success, although
                                                                                         it will check a non existing
                                                                                         element of the vector
        vector<char> v(5);
        for (int i=0; i<v.size(); ++i) ;
        cout << "Success!\n";


       15.string s = "Success!\n"; for (int i=0; i<6; ++i) cout << s[i];
            --> the string is longer than 6 characters
    string s = "Success!\n";
    for (int i=0; i<9; ++i) cout << s[i];


       16.if (true) then cout << "Success!\n"; else cout << "Fail!\n"; --> "then" is wrong and not needed

            if (true) cout << "Success!\n"; else cout << "Fail!\n";



       17.int x = 2000; char c = x; if (c==2000) cout << "Success!\n";  --> c is not an integer, 2000 is not a char

    int x = 2000;
    int c = x;
    if (c==2000) cout << "Success!\n";


       18.string s = "Success!\n"; for (int i=0; i<10; ++i) cout << s[i]; --> \n counts as a single character
            string s = "Success!\n";
            for (int i=0; i<9; ++i) cout << s[i];


       19.vector v(5); for (int i=0; i<=v.size(); ++i) ; cout << "Success!\n";  --> missing vector's type
            vector<int> v(5); // could be any type, int is just an example
            for (int i=0; i<=v.size(); ++i) ;
            cout << "Success!\n";


       20.int i=0; int j = 9; while (i<10) ++j; if (j<i) cout << "Success!\n"; --> i will never reach 10, j will not be
                                                                                   less than i
            int i=0;
            int j = 9;
            while (i<10) ++i;
            if (j<i) cout << "Success!\n";


       21.int x = 2; double d = 5/(x–2); if (d==2*x+0.5) cout << "Success!\n";


    int x = 2;
    double d = 5/ 2.0;
    if (d==x+0.5) cout << "Success!\n";



       22.string<char> s = "Success!\n"; for (int i=0; i<=10; ++i) cout << s[i];  --> <char> is not needed


    string s = "Success!\n";
    for (int i=0; i<9; ++i) cout << s[i];



       23.int i=0; while (i<10) ++j; if (j<i) cout << "Success!\n"; --> 'j' was not declared, 'i' should be increased
                                                                        to let 'j' be lower


    int i=0;
    int j=0;
    while (i<10) ++i;
    if (j<i) cout << "Success!\n";


       24.int x = 4; double d = 5/(x–2); if (d=2*x+0.5) cout << "Success!\n"; --> condition in if is false

       int x = 4; double d = 5/(x-2);
       if (d!=2*x+0.5) cout << "Success!\n";


       25.cin << "Success!\n";   --> cin is not an output
             cout << "Success!\n";
*/
    return 0;

    }