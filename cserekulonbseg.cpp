#include <iostream>

int main(){

    int a = 2;
    int b = 3;

    std::cout<<"Csere elott A = "<<a<<" B = "<<b<<std::endl;

    a = a+b;
    b = a-b;
    a = a-b;

    std::cout<<"Csere utan A = "<<a<<" B = "<<b<<std::endl;

    return 0;
}