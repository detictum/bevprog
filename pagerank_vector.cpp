#include <iostream>
#include <math.h>
#include <vector>

void printResult(std::vector<double> results, int n);

double distance(std::vector<double> PR, std::vector<double> PRv);



int main(){

    std::vector<std::vector<double> > L {
            {0.0, 0.0, 1.0/3.0, 0.0},
            {1.0, 1.0/2.0, 1.0/3.0, 1.0},
            {0.0, 1.0/2.0, 0.0, 0.0},
            {0.0, 0.0, 1.0/3.0, 0.0}
    };

    std::vector<double> PR = {0.0, 0.0, 0.0, 0.0};
    std::vector<double> PRv = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0};


    for(;;)
    {
        for(int i = 0; i < PR.size(); i++)
            PR[i] = PRv[i];
        for(int i = 0; i < L.size(); i++)
        {

            double PRvTemp = 0.0;
            for(int j = 0; j < L.size(); j++){
                PRvTemp += L[i][j]*PR[j];
                PRv[i] = PRvTemp;
                }
            }
        
                    if(distance(PR, PRv) < 0.000001)
            break;
    }



    printResult(PR, 4);

    return 0;
}



void printResult(std::vector<double> results, int n){
    for(int i = 0; i < n; ++i){
        std::cout<<results[i]<<std::endl;
    }
}

double distance(std::vector<double> PR, std::vector<double> PRv){
    double result = 0.0;

    for(int i = 0; i < PR.size(); ++i){
        result += (PR[i] - PRv[i])*(PR[i]-PRv[i]);
    }
    return sqrt(result);
}